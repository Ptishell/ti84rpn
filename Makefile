AS	= sjasm
AFLAGS  = -i./inc
TILEM	= tilem2
ROM	= ti84rpn.rom
UPG     = ti84rpn.8xu
HEX	= $(ROM:.rom=.hex)

SRC	= main.asm
SRC	:= $(addprefix src/, $(SRC))

all:: $(UPG)

$(UPG): $(ROM)
	mktiupgrade -p -d TI-84+ -k build/0A.key $< $(UPG) 00

emul:: $(ROM)
	$(TILEM) -m ti84p -r $(ROM)

debug:: $(ROM)
	$(TILEM) -m ti84p -dr $(ROM)

$(ROM): $(SRC)
	$(AS) $(AFLAGS) $^

clean::
	$(RM) $(ROM) $(BIN)
