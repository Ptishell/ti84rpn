    code @ 0000h
rst:
    jp boot

    code @ 0026h
    db 0

    code @ 0038h
sysint:
    di
    xor  a
    out  (PORT_INT_MASK), a
    ld   a, (GLOBAL_SHIFT)
    and  a
    jr   z, 1f
    xor  a
    ld   (GLOBAL_SHIFT), a
    call suspend
1:  ei
    ret

    code @ 0053h
    jp boot

    code @ 0056h
    db 0xFF, 0xA5, 0xFF

test: dz "Test !"
