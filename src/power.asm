enable_on_int:
    ld   a, 1
    out  (PORT_INT_MASK), a
    ei
    ret

suspend:
    di
    im   1
    ld   a, 02h
    out  (PORT_LCD_CMD), a
    call enable_on_int
    halt
    ld   a, 03h
    out  (PORT_LCD_CMD), a
    ret
