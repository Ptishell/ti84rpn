    output ti84rpn.rom

    include <io.inc>
    include <lcd.inc>
    include <data.inc>

    defpage 0..63, 0, 16384

    page 0
    include "vector.asm"
    include "boot.asm"
    include "power.asm"
    include "lcd.asm"
    include "keyboard.asm"
    include "font.asm"

    page 63
    include "fake_boot.asm"
