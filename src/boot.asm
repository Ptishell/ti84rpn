boot:
    di
    ld   a, 0Ch
    out  (PORT_MMAP), a
    call lcd_init
    ld   a, 81h
    out  (PORT_MEMB), a
    xor  a
    ld   (GLOBAL_SHIFT), a
    call suspend
    ld   a, 0
    ld   b, 0
    ld   hl, hello
    call lcd_print_string
    ei

loop:
    call get_key
    and  1 << 5
    jr   nz, loop
    ld   a, 1
    ld   (GLOBAL_SHIFT), a
    call suspend
    jr   loop

hello:
    dz "ABCDEFGHIJKLMNOPQRSTUVWXYZ abcdefghijklmnopqrstuvwxyz0123456798+-*/!@#$%^&*()_=[]{}\\|;:'\",<>.?~`"
