get_key:
    ld   a, 0FFh
    out  (PORT_KEYBOARD), a
    ld   a, 0BFh
    out  (PORT_KEYBOARD), a
    in   a, (PORT_KEYBOARD)
    ret
