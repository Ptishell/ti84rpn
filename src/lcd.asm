macro wait_lcd
    ex   (sp), hl
    ex   (sp), hl
    inc  (hl)
    dec  (hl)
endmacro

lcd_init:
    ld   a, 02h
    out  (PORT_LCD_CMD),a
    wait_lcd
    ld   a, 01h
    out  (PORT_LCD_CMD),a
    wait_lcd
    ld   a, 05h
    out  (PORT_LCD_CMD),a
    wait_lcd
    ld   a, 03h
    out  (PORT_LCD_CMD),a
    ret

lcd_set_x:
    add   a, 20h
    out  (PORT_LCD_CMD),a
    wait_lcd
    ret

lcd_set_y:
    add   a, 80h
    out  (PORT_LCD_CMD),a
    wait_lcd
    ret

;; a : x
;; b : y
;; c : char
lcd_print_char:
    push ix
    push de
    ld     e, c
    ld     d, 0
    call lcd_set_x
    repeat 3
        sla  b
        sla  e
        rl   d
    endrepeat
    ld     ix, font
    add    ix, de
    ld   a, b
    call lcd_set_y
    repeat 8
1:
        in a, (PORT_LCD_CMD)
        rla
        jr c, 1b
        ld   a, (ix)
        out  (PORT_LCD_DATA), a
        inc  b
        inc  ix
    endrepeat
    pop de
    pop ix
    ret

;; a: x
;; b: y
;; hl: string
lcd_print_string:
    push de
    ld   d, a
    ld   e, b
2:  ld   a, (hl)
    and  a
    jr   z, 1f
    ld   c, a
    ld   a, d
    ld   b, e
    call lcd_print_char
    inc  hl
    inc  d
    ld   a, d
    cp   LCD_WIDTH
    jp   s, 3f
    ld   d, 0
    inc  e
    ld   a, e
    cp   LCD_HEIGHT
    jp   s, 3f
    ld   e, 0
3:  jr   2b
1:  pop  de
    ret
